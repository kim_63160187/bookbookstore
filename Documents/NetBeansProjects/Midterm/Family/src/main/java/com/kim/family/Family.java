/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.family;

/**
 *
 * @author ASUS
 */
public class Family {
    // กำหนดคุณสมบัติ
    private String name;
    private int age ;
    private String sex;
    private String hairColor;
    private String job;
 
 //กำหนดค่าตัวแปรให้เป็นconstructor   
public Family(String name, int age,String sex,String hairColor,String job){
        System.out.println("Family created");
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.hairColor = hairColor;
        this.job = job;

    }
//คนในครอบครัวเดินแล้วก็เดินทั้งวัน
public void walk(){
        System.out.println("People in Famliy walk");
    }

//คนในครอบครัวกินทุกที่ทุกเวลา
public void eat(){
        System.out.println("People in Famliy eat");
    }

//คนในครอบครัวนั่ง
public void sit(){
        System.out.println("People in Famliy sit");
    }

//คนในครอบครัวกินเสร็จแล้วง่วงนอน
public void sleep(){
        System.out.println("People in Famliy sleep");
    }

    
}
